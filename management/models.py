from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal

class BasicCurrencies(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=255)
    value = models.DecimalField(max_digits=20,decimal_places=2,default=Decimal('0.0000'))
    change = models.DecimalField(max_digits=20,decimal_places=2,default=Decimal('0.0000'))
    data_date = models.DateField(auto_now=True)