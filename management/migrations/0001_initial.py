# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BasicCurrencies',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('type', models.CharField(max_length=255)),
                ('value', models.DecimalField(default=Decimal('0.0000'), max_digits=20, decimal_places=2)),
                ('change', models.DecimalField(default=Decimal('0.0000'), max_digits=20, decimal_places=2)),
                ('data_date', models.DateField(auto_now=True)),
            ],
        ),
    ]
