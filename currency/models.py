
class LiveInfo(object):
    def __init__(self,type,buy,sell,diff,last_update, created=None):
        self.type=type
        self.buy=buy
        self.sell=sell
        self.diff=diff
        self.last_update=last_update

class News(object):
    def __init__(self,url,image,title, created=None):
        self.url = url
        self.image = image
        self.title =title
