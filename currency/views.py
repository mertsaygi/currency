# -*- coding: utf-8 -*-
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from management.models import *
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from serializers import *
import  datetime
from decimal import Decimal
import urllib
from models import *
from bs4 import BeautifulSoup


def main(request):
    return HttpResponseRedirect('/docs/')

@api_view(['GET'])
def basic_info(request):
    if request.method == 'GET':
        currencies = BasicCurrencies.objects.all()
        serializer = BasicInfoSerializer(currencies, many=True)
        return Response(serializer.data)

def basic_info_save(request):
    url = "http://www.doviz.com/"
    page = urllib.urlopen(url).read()
    soup = BeautifulSoup(page)
    div = soup.findAll("div", { "class" : "header-doviz" })
    li = div[0].findAll("li")
    queryset = BasicCurrencies.objects.filter(data_date__gte=datetime.datetime.today())
    print queryset.count()
    if(queryset.count()==0):
        for row in li:
            n = row.findAll("span", { "class" : "menu-row1" })[0].text
            v = row.findAll("span", { "class" : "menu-row2" })[0].text
            d = row.findAll("span", { "class" : "menu-row3" })[0].text
            c = BasicCurrencies(type=n,value=Decimal(v.replace(",",".")),change=Decimal(d.replace(",",".").replace("%","")))
            c.save()
        return HttpResponse("Completed Successfully")
    else:
        return HttpResponse("Already Fetched Today")

@api_view(['GET'])
def detail_info(request):
    url = "http://kur.doviz.com/"
    page = urllib.urlopen(url).read()
    soup = BeautifulSoup(page)
    div = soup.findAll("table", { "id" : "currencies" })
    li = div[0].findAll("tr")
    d = []
    i = 0
    for row in li:
        name = row.findAll("td", { "class" : "column-row6" })[0].text
        buy = row.findAll("td", { "class" : "column-row4" })[0].text
        sell = row.findAll("td", { "class" : "column-row4" })[1].text
        diff = row.findAll("td", { "class" : "column-row4" })[2].text
        last_update_date = row.findAll("td", { "class" : "column-row2" })[0].text
        if i != 0:
            s = LiveInfo(type = name.encode('ascii', 'ignore'),buy = Decimal(buy.replace(",",".")) ,sell =Decimal(sell.replace(",",".")) ,diff = Decimal(diff.replace(",",".").replace("%","")) ,last_update = last_update_date )
            d.append(s)
            print s.type
        i = i+ 1
    serializer = LiveInfoSerializer(d, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def share_info(request):
    url = "http://borsa.doviz.com/hisseler"
    page = urllib.urlopen(url).read()
    soup = BeautifulSoup(page)
    div = soup.findAll("div", { "class" : "doviz-column2" })
    return HttpResponse(div)

@api_view(['GET'])
def news_list(request):
    url = "http://haber.doviz.com/"
    page = urllib.urlopen(url).read()
    soup = BeautifulSoup(page)
    div = soup.findAll("div", { "class" : "doviz-column2" })
    d = []
    for f in div:
        name = f.p.text
        url = f.a.get('href')
        try:
            img = f.img['src']
        except:
            img = None
        s = News(url=url,image=img,title=name)
        d.append(s)
    serializer = NewsSerializer(d,many=True)
    return Response(serializer.data)
