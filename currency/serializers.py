# -*- coding: utf-8 -*-
from rest_framework import serializers
from management.models import *

class LiveInfoSerializer(serializers.Serializer):
    type = serializers.CharField(max_length=255)
    buy = serializers.DecimalField(max_digits=20,decimal_places=2,default=Decimal('0.0000'))
    sell = serializers.DecimalField(max_digits=20,decimal_places=2,default=Decimal('0.0000'))
    diff = serializers.DecimalField(max_digits=20,decimal_places=2,default=Decimal('0.0000'))
    last_update = serializers.DateField()

class NewsSerializer(serializers.Serializer):
    url = serializers.CharField(max_length=255)
    image = serializers.CharField(max_length=255)
    title = serializers.CharField(max_length=255)

class BasicInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = BasicCurrencies
        fields = ('id','type','value','change','data_date')
